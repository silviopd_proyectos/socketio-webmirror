// client interaction functionality setup
//window.onload = function() {
$(document).ready(function() {
  var socket = io.connect("https://socketio-webmirror.herokuapp.com/");

  // send mouse position updates
  document.onmousemove = function(ev) {
    socket.emit("mouse movement", { pos: { x: ev.clientX, y: ev.clientY } });
  };

  // initial setup, should only happen once right after socket connection has been established
  socket.on("mouse setup", function(mouses) {
    for (var mouse_id in mouses) {
      virtualMouse.move(mouse_id, mouses.mouse_id);
    }
  });

  // update mouse position
  socket.on("mouse update", function(mouse) {
    virtualMouse.move(mouse.id, mouse.pos);
  });

  // remove disconnected mouse
  socket.on("mouse disconnect", function(mouse) {
    virtualMouse.remove(mouse.id);
  });

  /*
  $("#myVideo").click(function() {    
    $("#myVideo").get(0).paused ? $("#myVideo").get(0).play() : $("#myVideo").get(0).pause();
      socket.emit("clicked");
  });
  */

  //when we receive numClients, do this
  socket.on("buttonUpdate", function() {
    $("#myVideo").get(0).paused ? $("#myVideo").get(0).play() : $("#myVideo").get(0).pause();
  });
  
  var video = new Vue({
    el: '#form',
    data: {
      video: "",
      message: "https://1fiafzt.oloadcdn.net/dl/l/1-jIYRy_sA1KlQ8N/l-xH-ARH46U/The.commuter.2018.1080p-dual-lat-cinecalidad.to.mp4?mime=true"
    },  
    methods: {
      changeVideo(){
        this.video = this.message;        
        socket.emit("change",this.message);
      },
      clear(){
        this.message = "";
      },
      clickvideo(){
        $("#myVideo").get(0).paused ? $("#myVideo").get(0).play() : $("#myVideo").get(0).pause();
        socket.emit("clicked");
      },
      fullscreenVideo(){
        var elem = document.getElementById("myVideo");
        if (elem.requestFullscreen) {
          elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) {
          elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
          elem.webkitRequestFullscreen();
        }
        socket.emit("fullscreen");
      },
      salirVideo(){

      }
    }
  });

  //when we receive numClients, do this
  socket.on("buttonVideo", function(data) {
    $("#myVideo").attr("src",data);
  });

  //when we receive numClients, do this
  socket.on("buttonfullscreen", function() {
    console.log("fullscreen");
    var elem = document.getElementById("myVideo");
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    }
  });
});

// virtual mouse module
var virtualMouse = {
  // moves a cursor with corresponding id to position pos
  // if cursor with that id doesn't exist we create one in position pos
  move: function(id, pos) {
    var cursor = document.getElementById("cursor-" + id);
    if (!cursor) {
      cursor = document.createElement("img");
      cursor.className = "virtualMouse";
      cursor.id = "cursor-" + id;
      cursor.src = "/img/cursor.png";
      cursor.style.position = "absolute";
      document.body.appendChild(cursor);
    }
    cursor.style.left = pos.x + "px";
    cursor.style.top = pos.y + "px";
  },
  // remove cursor with corresponding id
  remove: function(id) {
    var cursor = document.getElementById("cursor-" + id);
    cursor.parentNode.removeChild(cursor);
  }
};
